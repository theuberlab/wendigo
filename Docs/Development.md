# Development

This is where we define development items such as coding standards


# Components

## Core
Wendigo is like this || big so for now this is the only component.
Later things will change


# Milestones
We should have milestones

### Working Hello World
Target for version:
0.0.1

### Generate CPU Load
Target for version:
0.1.0

### Generate Memory Load
Target for version:
0.3.0

### Change Load generation during runtime
Target for version:
0.4.0

### Swagger for (the teeny tiny) API
Target for version:
0.9.0



### Clustering?
Target for version:
WAAAAAY later


# Versions
What do various versions mean.

## 0.1.0
Minimal functionality as a CPU load generator

## 0.2.0
Configurable during runtime

## 0.3.0
Generate memory load as well

## 0.9.0
Tell the world about it?

## 1.0.0

Includes all the following milestones:

* [ ] Working Hello World
* [ ] Generate CPU Load
* [ ] Generate Memory Load
* [ ] Change Load generation during runtime
* [ ] Swagger for (the teeny tiny) API