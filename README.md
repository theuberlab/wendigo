# Wendigo #
Wendigo is a tool which will consume varying amounts of system resources. This can be used to generate server load for testing.

Initially it will be limited to CPU and memory.

The Consumer will support both a startup configuration and modification during runtime via a web interface.
It will also provide a monitoring endpoint in prometheus format for fetching statistics.

Initally The Consumer will run as a stand-alone binary but it would be nice to add some small clustering functionality so that one could change the CPU consumption of all instances at the same time.

This is being written to test autoscaling functionality in kubernetes clusters so perhaps some built in support of that (like writing to a configmap if that's still possible in k8s.)

# Contributing
Pull requests are always welcome

# Building
Once there's code 

```go build```

At some point after that I'll be publishing a container image so that this will actually be useful.

# Name
Wendigo is named after the [Algonquain monster of the same name](https://en.wikipedia.org/wiki/Wendigo) historically associated with insatiable greed.