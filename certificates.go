package main

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"log"
	"math/big"
	"time"
)

// Generate a private/public key pair for the specified key size.
func genKeyPair(keySize int) (*rsa.PrivateKey, *rsa.PublicKey){
	reader := rand.Reader

	privateKey, err := rsa.GenerateKey(reader, keySize)
	if err != nil {
		log.Fatalln(err)
	}

	publicKey := privateKey.PublicKey

	return privateKey, &publicKey
}

// Generates a certificate for the provided key pair. Is currently returned as a []byte containing the certificate in PEM format.
func genCert(privKey *rsa.PrivateKey, pubKey *rsa.PublicKey) []byte {
	notBefore := time.Now()

	notAfter := notBefore.Add(365 * 24 * time.Hour)

	serialNumberLimit := new(big.Int).Lsh(big.NewInt(1), 128)
	serialNumber, err := rand.Int(rand.Reader, serialNumberLimit)
	if err != nil {
		log.Fatalf("failed to generate serial number: %s", err)
	}

	template := x509.Certificate{
		SerialNumber: serialNumber,
		Subject: pkix.Name{
			Organization: []string{"Nordstrom"},
		},
		NotBefore: notBefore,
		NotAfter:  notAfter,

		KeyUsage:              x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth},
		BasicConstraintsValid: true,
	}

	template.DNSNames = append(template.DNSNames, "localhost")
	template.DNSNames = append(template.DNSNames, "localhost.localdomain")

	derBytes, err := x509.CreateCertificate(rand.Reader, &template, &template, pubKey, privKey)
	if err != nil {
		log.Fatalf("Failed to create certificate: %s", err)
	}

	pemBytes := pem.EncodeToMemory(&pem.Block{Type: "CERTIFICATE", Bytes: derBytes})

	return pemBytes
}