package main

import (
	"bitbucket.org/theuberlab/lug"
	"crypto/tls"
	"crypto/x509"
	"encoding/base64"
	"encoding/json"
	"encoding/pem"
	"fmt"
	"github.com/golang/glog"
	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gopkg.in/alecthomas/kingpin.v2"
	"log"
	"net/http"
	"os"
	"runtime"
	"strconv"
	"time"
)

// ###############################
// Variables
// ###############################

// Global Variables.
var (
	applicationVersion = "0.2.1" //TODO: Move this to a separate file so that it's easier to automatically munge.
	promNS             = "wendigo"
)

// Variables that will be used as handles for specific activities.
var (
	// Creates the top level context for all commands flags and arguments
	app         		=	kingpin.New("wendigo", "Consumes system resources (CPU and memory.)")
	stopchan    			chan bool
	stoppedchan				chan bool
)

// Runtime configration
var (
	listenPort        		=	8443
	threads              	=	runtime.NumCPU()
	loadruntime   				time.Duration
	configfile    				string
	lugConfigFile				string
	keyPEM        				[]byte
	certPEM       				[]byte
	loadStartTime				time.Time
	loadEndTime   				time.Time
	loadRunning					bool
)

// Metrics
var (
	promLoadRunning = prometheus.NewGaugeFunc(prometheus.GaugeOpts{
		Name:      "load_running",
		Help:      "1 if the test is currently running.",
		Namespace: promNS,
	}, isRunning)

	promThreads = prometheus.NewGauge(prometheus.GaugeOpts{
		Name:      "num_threads",
		Help:      "The number of threads running for the CPU load generator.",
		Namespace: promNS,
		Subsystem: "cpu",
	})

	promLoadRunDuration = prometheus.NewGauge(prometheus.GaugeOpts{
		Name:      "run_duration_seconds",
		Help:      "How long in seconds load generation will run.",
		Namespace: promNS,
	})

	promLoadRunTime = prometheus.NewGaugeFunc(prometheus.GaugeOpts{
		Name:      "run_time_seconds",
		Help:      "How long in seconds since load generation was started.",
		Namespace: promNS,
	}, getRunTimeSecs)

	promStartTime = prometheus.NewGauge(prometheus.GaugeOpts{
		Name:      "start_time_seconds",
		Help:      "Time in seconds that the last load test was started.",
		Namespace: promNS,
	})
)

// ###############################
// Types
// ###############################

// Format the web response
type Message struct {
	Message string
}

func init() {
	loadStartTime = time.Now()

	// Setup Kingpin flags
	// Set the application version number
	app.Version(applicationVersion)

	// Allow -h as well as --help
	app.HelpFlag.Short('h')

	// Add the flags
	app.Flag("config", "Load config from a file instead of flags or environment variables. Can be set via WENDIGO_CONNFIG.").Short('c').Default("config").Envar("WENDIGO_CONNFIG").StringVar(&configfile)
	app.Flag("threads", "The number of threads to start with. Can be set via WENDIGO_THREADS.").Short('t').Envar("WENDIGO_THREADS").IntVar(&threads)
	app.Flag("lug-config", "Specify a LUG config file. Can be set via WENDIGO_LUG_CONNFIG.").Short('l').Default("lugconfig.yaml").Envar("WENDIGO_LUG_CONNFIG").StringVar(&lugConfigFile)
	app.Flag("loadruntime", "How long to generate load for. A value of 0 will cause wendigo to run indefinately. Can be set via WENDIGO_RUN_TIME.").Short('r').Default("10s").Envar("WENDIGO_RUN_TIME").DurationVar(&loadruntime)

	// Now parse the flags
	kingpin.MustParse(app.Parse(os.Args[1:]))

	// Initialize Lug
	lug.InitLoggerFromYaml(lugConfigFile)
	lug.Info("Message", "Initialized LUG")

	// Create the inter-thread communications channels
	stopchan = make(chan bool)
	// a channel to signal that it's stopped
	stoppedchan = make(chan bool)

	keyPEMStr := os.Getenv("WENDIGO_PRIVATE_KEY")
	certPEMStr := os.Getenv("WENDIGO_CERTIFICATE")

	// If either of those are empty
	if (keyPEMStr == "" || certPEMStr == "") {
		// Generate a default key and cert
		privKey, pubKey := genKeyPair(2048)

		keyPEM = pem.EncodeToMemory(&pem.Block{Type: "RSA PRIVATE KEY", Bytes: x509.MarshalPKCS1PrivateKey(privKey)})
		certPEM = genCert(privKey, pubKey)
	} else {
		pemStr, err := base64.StdEncoding.DecodeString(keyPEMStr)
		if err != nil {
			glog.Fatalln(err)
		}
		certStr, err := base64.StdEncoding.DecodeString(certPEMStr)
		if err != nil {
			glog.Fatalln(err)
		}

		keyPEM = []byte(pemStr)
		certPEM = []byte(certStr)
	}

	// Initialize Prometheus
	prometheus.MustRegister(promThreads)
	prometheus.MustRegister(promLoadRunning)
	prometheus.MustRegister(promLoadRunDuration)
	prometheus.MustRegister(promStartTime)
	prometheus.MustRegister(promLoadRunTime)

	setCPUThreads(threads)
	setRunTime(loadruntime)
	promStartTime.Set(float64(loadStartTime.Second()))
}

// ###############################
// Code
// ###############################

func isRunning() float64 {
	if loadRunning {
		return 1
	} else {
		return 0
	}
}

// Start the configured number of threads.
func consumeCPU() {
	fmt.Println("Starting threads")

	for i := 0; i < threads; i++ {
		go loadFunction(i)
	}

	fmt.Println("All threads started")
	loadRunning = true

	if loadruntime == 0 {
		// Block forever if runtime is 0
		select{}
	} else {
		time.Sleep(loadruntime)
	}

	for i := 0; i < threads; i++ {
		stopchan <- true
	}
}

// elapsed time for the last (or current) load generation.
func getRunTimeSecs() float64 {
	if loadRunning {
		return float64(time.Since(loadStartTime).Seconds())
	} else {

		return float64(loadEndTime.Sub(loadStartTime).Seconds())
	}


}

// This function will actually generate some load.
func loadFunction(thisThread int) {

	defer func() {
		fmt.Println("Cleaning up thread: " + strconv.Itoa(thisThread))
		stoppedchan <- true
	}()

	fmt.Println("Starting thread:" + strconv.Itoa(thisThread))

	for {
		select {
		default:
			//fmt.Println("Looping thread:" + strconv.Itoa(thisThread))
			//time.Sleep(1 * time.Second)
		case <-stopchan:
			fmt.Println("Stopping thread: " + strconv.Itoa(thisThread))
			// stop
			return
		}
	}
}

// Set the number of threads to run the CPU load on
func setCPUThreads(newThreads int) {
	threads = newThreads
	promThreads.Set(float64(newThreads))
}

// Update how long load generation will run for.
func setRunTime(newRun time.Duration) {
	loadruntime = newRun
	promLoadRunDuration.Set(float64(newRun.Seconds()))
}

// Handle the API request for starting the load test.
func reqStartLoad(w http.ResponseWriter, r *http.Request) {
	//lug.LogDebug("Message", "Stop interface called.")
	aMessage := Message{
		Message: "Load generation starting.",
	}

	startLoad()
	json.NewEncoder(w).Encode(aMessage)
}

// Do the internal stuff neccessary to start the load generation
func startLoad() {
	promStartTime.Set(float64(loadStartTime.Unix()))
	loadStartTime = time.Now()
	loadEndTime = loadStartTime

	go consumeCPU()
}

// Handle the API request for stopping the load test.
func reqStopLoad(w http.ResponseWriter, r *http.Request) {
	//lug.LogDebug("Message", "Stop interface called.")
	aMessage := Message{
		Message: "Load generation stopping.",
	}

	stopLoad()
	json.NewEncoder(w).Encode(aMessage)
}

// Do the internal stuff neccessary to stop the load generation
func stopLoad() {
	for i := 0; i < threads; i++ {
		stopchan <- true
	}
	loadRunning = false
	loadEndTime = time.Now()
}

func main() {
	
	router := mux.NewRouter()

	// Configure TLS
	cer, err := tls.X509KeyPair(certPEM, keyPEM)
	if err != nil {
		glog.Fatalln(err)
	}
	config := &tls.Config{Certificates: []tls.Certificate{cer}}
	listener, err := tls.Listen("tcp", fmt.Sprintf(":%d", listenPort), config)
	if err != nil {
		glog.Fatalln(err)
	}

	router.HandleFunc("/start", reqStartLoad).Methods("GET")
	router.HandleFunc("/stop", reqStopLoad).Methods("GET")

	// Handle prometheus metrics
	router.Handle("/metrics", promhttp.Handler())

	fmt.Println("running")
	startLoad()

	// Start a web service
	log.Fatal(http.Serve(listener, router))
}
